import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class BookPurchaseForm extends Component {

    render() {
        var partial = '';
        if (this.props.sResponse === 1) {
            partial = <div className="row">
                <div className="col s3">
                </div>
                <div className="col s6">
                    <div className="alert alert-success">
                        Book purchased successfully! <Link to="/" >See purchased Books</Link>
                    </div>
                    <div className="col s3">
                    </div>
                </div>
            </div>
        }
        else if (this.props.sResponse === 0) {
            partial = <div className="row">
                <div className="col s3">
                </div>
                <div className="col s6">
                    <div className="alert alert-danger">
                        <strong>Error!</strong> an error occurred please try again later.
                    </div>
                    <div className="col s3">
                    </div>
                </div>
            </div>
        }

        return (
            <div id="Form" className="col s12">
                <p />

                {partial}

                <form onSubmit={this.props.handleFormSubmit}>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <label htmlFor="body">
                                <strong style={{fontSize: '20px'}}>Title: </strong>
                                <Link to={`/books/${this.props.book.id}`}>
                                    {this.props.book.title}
                                </Link>
                                <input id="book_id" value={this.props.book.id} type="hidden" name="id"/>
                            </label>
                        </div>
                        <div className="col s2">
                        </div>
                        <div />
                    </div>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <label htmlFor="title">
                                <input id="email" className="validate" value={this.props.email}
                                        type="text" name="email"
                                        onChange={this.props.handleInputChange}
                                       placeholder="Email"/>
                            </label>
                        </div>
                        <div className="col s2">
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <button type="submit" className="btn waves-effect waves-light" value="Submit">Buy Now
                                <i className="material-icons right"></i>
                            </button>
                        </div>
                        <div className="col s2">
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
