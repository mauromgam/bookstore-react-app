import React, { Component } from 'react';

export default class LoginForm extends Component {

    render() {
        return (
            <div id="Form" className="col s12">
                <p />
                <form onSubmit={this.props.handleFormSubmit}>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <label htmlFor="title">
                                <input id="email" className="validate" value={this.props.email}
                                        type="text" name="email"
                                        onChange={this.props.handleInputChange}
                                        placeholder="Email"/>
                            </label>
                        </div>
                        <div className="col s2">
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <label htmlFor="body">
                                <input id="title" value={this.props.password}
                                        type="password" name="password"
                                        onChange={this.props.handleInputChange}
                                        placeholder="Password"/>
                            </label>
                        </div>
                        <div className="col s2">
                        </div>
                        <div />
                    </div>
                    <div className="row">
                        <div className="col s2">
                        </div>
                        <div className="col s8">
                            <button type="submit" className="btn waves-effect waves-light" value="Submit">Login
                                <i className="material-icons right"></i>
                            </button>
                        </div>
                        <div className="col s2">
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
