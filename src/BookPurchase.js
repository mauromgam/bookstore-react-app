import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import BookPurchaseForm from './BookPurchaseForm';

export default class BookPurchase extends Component {
    constructor(props) {
        super(props);
        const email = localStorage.getItem('email');

        this.state = {
            book: {
                id: 0,
                title: ''
            },
            email: (email && email.length) ? email : '',
            responseMsg: -1,
            redirect: false,
            loading: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    getBook = async () => {
        const { id } = this.props.match.params;
        this.setState({loading: true});
        await axios.get('http://localhost:8000/api/books/' + id)
            .then(response => {
                if (response.data.data) {
                    this.setState({
                        book: response.data.data,
                        loading: false
                    });
                }
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                });
            });
    };

    async componentDidMount() {
        this.getBook();
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    };

    handleInputChange = (event) => {
        this.setState({ responseMsg: -1 });
        let input = event.target;
        let name = event.target.name;
        let value = input.value;

        this.setState({
            [name]: value
        });
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        axios.post('http://localhost:8000/api/purchases', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                email: this.state.email,
                book_id: this.state.book.id
            })
            .then((response) => {
                this.setState({loading: false});
                if (response.data.success) {
                    this.setState({
                        responseMsg: 1,
                        redirect: true
                    });
                }
                else {
                    this.setState({ responseMsg: 0 })
                }
            })
            .catch((error) => console.log(error));
    };

    render() {
        if (this.state.loading) return <div className="loader"/>;
        return (
            <div>
                {this.renderRedirect()}
                <div className="col s12">
                    <div className="col s2">

                    </div>
                    <div className="col s8">
                        <BookPurchaseForm handleFormSubmit={this.handleFormSubmit}
                                     handleInputChange={this.handleInputChange}
                                      book={this.state.book}
                                      email={this.state.email}
                                     sResponse={this.state.responseMsg} />
                    </div>
                    <div className="col s2">

                    </div>
                </div>
            </div>
        );
    };
}
