import React, { Component } from 'react';
import axios from 'axios';
import CustomPaginationActionsTable from './CustomPaginationActionsTable';

export default class BookList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: null,
            totalCount: 0,
            rowsPerPage: props.rowsPerPage ? props.rowsPerPage : 15,
            activePage: 0,
            loading: false
        };

        this.setActivePage=this.setActivePage.bind(this);
        this.setRowsPerPage=this.setRowsPerPage.bind(this);
    }

    setActivePage = (activePage) => {
        this.setState({ activePage: activePage });
    };

    setRowsPerPage = (rowsPerPage) => {
        this.setState({
            rowsPerPage: rowsPerPage,
            activePage: 0
        });
    };

    getBooks = async () => {
        const { rowsPerPage, activePage } = this.state;
        this.setState({loading: true});

        const recentlyViewed = this.props.recentlyViewed ? this.props.recentlyViewed : null;
        const email = this.props.email ? this.props.email : null;
        const endpoint = this.props.endpoint ? this.props.endpoint : 'books';

        await axios.get(`http://localhost:8000/api/${endpoint}`, {
            params: {
                page_size: rowsPerPage,
                page: (parseInt(activePage) + 1),
                ids: recentlyViewed,
                email: email
            }
        })
            .then(response => {
                if (response.data.data && response.data.data.data) {
                    this.setState({
                        books: response.data.data.data,
                        totalCount: response.data.data.total,
                        loading: false
                    });
                }
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                });
            });
    };

    async componentDidMount() {
        this.getBooks();
        this.setState({
            loading: false
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.activePage !== this.state.activePage ||
            prevState.rowsPerPage !== this.state.rowsPerPage
        ) {
            this.getBooks();
        }
    }

    render() {
        return <div>
            <h3>{this.props.pageTitle ? this.props.pageTitle : "Book List"}</h3>
            <CustomPaginationActionsTable
                rows={this.state.books ? this.state.books : []}
                loading={this.state.loading}
                page={this.state.activePage}
                rowsPerPage={this.state.rowsPerPage}
                rowsPerPageOptions={this.props.rowsPerPageOptions ? this.props.rowsPerPageOptions : null}
                count={this.state.totalCount}
                setActivePage={this.setActivePage}
                setRowsPerPage={this.setRowsPerPage}
                emptyMessage={this.props.emptyMessage ? this.props.emptyMessage : 'No books found.'}
                recentlyViewedList={this.props.recentlyViewedList}
            />
        </div>;
    }
};