import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import LoginForm from './LoginForm';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            responseMsg: -1,
            redirect: false,
            loading: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    };

    handleInputChange = (event) => {
        this.setState({ responseMsg: -1 });
        let input = event.target;
        let name = event.target.name;
        let value = input.value;

        this.setState({
            [name]: value
        });
    };

    handleFormSubmit = (event) => {
        event.preventDefault();
        localStorage.setItem('email', this.state.email);

        this.setState({
            redirect: true
        });
    };

    render() {
        return (
            <div >
                {this.renderRedirect()}
                <div className="col s12">
                    <div className="col s2">

                    </div>
                    <div className="col s8">
                        <LoginForm handleFormSubmit={this.handleFormSubmit}
                                handleInputChange={this.handleInputChange}
                                email={this.state.email}
                                password={this.state.password}
                                sResponse={this.state.responseMsg} />
                    </div>
                    <div className="col s2">

                    </div>
                </div>
            </div>
        );
    };
}
