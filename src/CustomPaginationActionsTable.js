import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="First Page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Previous Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Next Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Last Page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
    TablePaginationActions,
);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 500,
        fontSize:'14px',
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

class CustomPaginationActionsTable extends React.Component {
    handleChangePage = (event, page) => {
        this.setState({ page });
        if (this.props.setActivePage) {
            this.props.setActivePage(page);
        }
    };

    handleChangeRowsPerPage = event => {
        this.setState({ page: 0, rowsPerPage: event.target.value });
        if (this.props.setRowsPerPage) {
            this.props.setRowsPerPage(event.target.value);
        }
    };

    printRows = (rows) => {
        let viewedBooks = JSON.parse(localStorage.getItem('viewedBooks'));
        let { recentlyViewedList } = this.props;
        return rows.map(row => (
            <TableRow key={`${row.id}_${row.timestamp ? row.timestamp : null}`}
                      className={
                          (!recentlyViewedList && viewedBooks && viewedBooks.indexOf(row.id) !== -1) ?
                          'viewed' :
                          null
                      }
            >
                <TableCell component="th" scope="row">
                    {row.title}
                </TableCell>
                <TableCell>{row.info.substring(0, 150)}{row.info.length > 150 ? '...' : null}</TableCell>
                <TableCell>
                    <Link to={"/books/" + row.id}>
                        <button className="buttonViewDetails">More Info</button>
                    </Link>
                </TableCell>
            </TableRow>
        ));
    };

    printEmptyRow = () => {
        return (
            <TableRow>
                <TableCell className="emptyMessage" colSpan={3}>{ this.props.emptyMessage ? this.props.emptyMessage : 'Empty' }</TableCell>
            </TableRow>
        );
    };

    printLoadingRow = () => {
        return (
            <TableRow>
                <TableCell component="th" scope="row" colSpan={2}>
                    <div className="loader" style={{position: 'relative'}} />
                </TableCell>
            </TableRow>
        );
    };

    render() {
        const { classes } = this.props;
        let { rows, count, page, rowsPerPage, loading } = this.props;
        count = parseInt(count);
        page = parseInt(page);
        rowsPerPage = parseInt(rowsPerPage);

        return (
            <Paper className={classes.root}>
                <div className={`${classes.tableWrapper}`}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>Title</CustomTableCell>
                                <CustomTableCell>Info</CustomTableCell>
                                <CustomTableCell/>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                loading ?
                                    this.printLoadingRow() :

                                    (rows.length > 0 ?
                                        this.printRows(rows) :
                                        this.printEmptyRow()
                                    )
                            }
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={this.props.rowsPerPageOptions ?
                                        this.props.rowsPerPageOptions :
                                        [15, 25, 50]
                                    }
                                    colSpan={3}
                                    count={count}
                                    rowsPerPage={parseInt(rowsPerPage)}
                                    page={parseInt(page)}
                                    SelectProps={{
                                        native: true,
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActionsWrapped}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </Paper>
        );
    }
}

CustomPaginationActionsTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomPaginationActionsTable);