import React, { Component } from 'react';
import RecentlyViewed from "./RecentlyViewed";
import RecentlyPurchased from "./RecentlyPurchased";

export default class Home extends Component {
    render() {
        return (<div>
            <RecentlyPurchased />
            <RecentlyViewed />
        </div>);
    }
};