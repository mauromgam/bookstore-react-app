import React, { Component } from 'react';
import { Container, Menu } from 'semantic-ui-react';

export default class Navbar extends Component {
    constructor(props) {
        super(props);

        const email = localStorage.getItem('email');

        this.state = {
            authenticated: !!(email && email.length),
            user: null
        };

        this.logout = this.logout.bind(this);
    }

    logout = () => {
        localStorage.clear();
        this.setState({
            user: null,
            authenticated: false
        });
        window.location.reload();
    };

    async componentDidUpdate(prevProps, prevState, snapshot) {
        const email = localStorage.getItem('email');

        if (prevState.user !== email) {
            this.setState({
                authenticated: !!(email && email.length),
                user: email
            });
        }
    }

    render() {
        return (
            <div>
                <Menu fixed="top" inverted>
                    <Container>
                        <Menu.Item as="a" header href="/">Home</Menu.Item>
                        {<Menu.Item id="books-button" header as="a" href="/books">Books</Menu.Item>}
                        {this.state.authenticated === true ? <Menu.Item id="logout-button" as="a" onClick={this.logout}>Logout</Menu.Item> : null}
                        {this.state.authenticated === false ? <Menu.Item as="a" href="/login">Login</Menu.Item> : null}
                    </Container>
                </Menu>
            </div>
        );
    }
};