import React, { Component } from 'react';
import BookList from "./BookList";

export default class RecentlyPurchased extends Component {
    email = localStorage.getItem('email');

    render() {
        return <BookList
            email={this.email}
            rowsPerPage={5}
            endpoint="recent-purchases"
            rowsPerPageOptions={[5, 10, 25]}
            pageTitle="Recently Purchased"
            emptyMessage={this.email ? "The list is empty." : "Please login to see recently purchased books."}
            recentlyViewedList={true}
        />;
    }
};