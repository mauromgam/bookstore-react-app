import React, { Component } from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";

export default class BookDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            book: null,
            loading: false,
            id: 0
        };
    }

    saveViewedBook = (id) => {
        let viewedBooks = JSON.parse(localStorage.getItem('viewedBooks'));
        if (!viewedBooks) {
            viewedBooks = [id];
        } else if (viewedBooks.indexOf(id) === -1) {
            viewedBooks.push(id);
        }
        localStorage.setItem('viewedBooks', JSON.stringify(viewedBooks));
    };

    getBook = async () => {
        const { id } = this.props.match.params;
        this.setState({loading: true});
        await axios.get('http://localhost:8000/api/books/' + id)
            .then(response => {
                if (response.data.data) {
                    const book = response.data.data;
                    this.saveViewedBook(book.id);

                    this.setState({
                        book: book,
                        loading: false
                    });
                }
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    loading: false
                });
            });
    };

    async componentDidMount() {
        this.getBook();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.id !== this.state.id) {
            this.getBook();
        }
    }

    render() {
        const book = this.state.book;
        if (!book || this.state.loading) return <div>Loading...</div>;
        return <div className="col s12">
            <div className="col s6">
                <div className="z-depth-3">
                    <div className="card horizontal">
                        <div className="card-image">
                            <img src="/img/react_book_cover.jpg" alt=""/>
                        </div>
                        <div className="card-stacked">
                            <div className="card-content">
                                <h5 className="card-title">{book.title}</h5>
                                <p>{book.info}</p>
                            </div>
                            <div className="card-action">
                                <Link to={"/books/" + book.id + "/purchase"} book={book}>
                                    <button className="buttonViewDetails">Buy</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s3">
            </div>
        </div>;
    }
};