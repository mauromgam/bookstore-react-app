import React, { Component } from 'react';
import BookList from "./BookList";

export default class RecentlyViewed extends Component {
    recentlyViewed = JSON.parse(localStorage.getItem('viewedBooks'));

    render() {
        return <div style={{marginTop: '50px'}}>
            <BookList
                recentlyViewed={this.recentlyViewed}
                rowsPerPage={5}
                endpoint="recently-viewed"
                rowsPerPageOptions={[5, 10, 25]}
                pageTitle="Recently Viewed"
                emptyMessage="The list is empty."
                recentlyViewedList={true}
            />
        </div>;
    }
};