import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import './index.css';

import Navbar from './Navbar';
import Home from './Home';
import BookList from './BookList';
import BookDetail from "./BookDetail";
import BookPurchase from "./BookPurchase";
import Login from "./Login";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Navbar />
                    <Container text style={{ marginTop: '7em' }}>
                        <Route path="/" exact component={Home} />
                        <Route path="/login" exact component={Login} />
                        <Route path="/books" exact component={BookList} />
                        <Route path="/books/:id" exact component={BookDetail} />
                        <Route path="/books/:id/purchase" exact component={BookPurchase} />
                    </Container>
                </div>
            </Router>
        );
    }
}

export default App