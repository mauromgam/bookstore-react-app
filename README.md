## Installation

**Note: make sure you have the Laravel API installed before running this app.**

`cd` the project directory and run `npm install`

## Start App

`cd` the project directory and run `yarn start`